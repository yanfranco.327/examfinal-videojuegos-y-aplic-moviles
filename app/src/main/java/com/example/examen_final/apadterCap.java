package com.example.examen_final;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class apadterCap extends RecyclerView.Adapter<apadterCap.AdapterCotacViewHolder> {

 //Recibimos la lista por el constructor y el contexto.

    List<pokemon> list;
    Context mContext;

    public apadterCap(List<pokemon> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterCotacViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterCotacViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_cap, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCotacViewHolder holder, int position) {

        holder.SetInfo(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class AdapterCotacViewHolder extends RecyclerView.ViewHolder {
        //Creancion de variable.
        ImageView imageURl;
        TextView name;
        TextView tipo;

        public AdapterCotacViewHolder(@NonNull View itemView) {
            super(itemView);
            imageURl = itemView.findViewById(R.id.imageURL);
            name = itemView.findViewById(R.id.name);
            tipo = itemView.findViewById(R.id.tipo);
        }

        void SetInfo(pokemon aClass) {
            String im = "https://upn.lumenes.tk" + aClass.getUrl_imagen(); //Trae la image.
            Picasso.get() //Seteando con picaso.
                    .load(im)
                    .into(imageURl);
            name.setText(aClass.getNombre());
            tipo.setText(aClass.getTipo());
        }
    }
}

package com.example.examen_final;

public class modelo {

    String nombres;
    String pueblo;
    String imagen;

    public modelo() {

    }

    public modelo(String imagen, String nombres, String pueblo) {
        this.imagen = imagen;
        this.nombres = nombres;
        this.pueblo = pueblo;
    }


    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPueblo() {
        return pueblo;
    }

    public void setPueblo(String pueblo) {
        this.pueblo = pueblo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}

package com.example.examen_final;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ImageView imagen;
    TextView nombres;
    TextView text;
    TextView pueblo;
    Button registrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagen = findViewById(R.id.imgaen);
        nombres = findViewById(R.id.nombres);
        pueblo = findViewById(R.id.pueblo);
        registrar = findViewById(R.id.registrar);
        registrar.setEnabled(false);
        text = findViewById(R.id.text);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/entrenador/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        servicio service = retrofit.create(servicio.class);

        Call<modelo> getEntrenador = service.getEntrenador();
        getEntrenador.enqueue(new Callback<modelo>() {
            @Override
            public void onResponse(Call<modelo> call, Response<modelo> response) {

                modelo entrenador = response.body();
                assert entrenador != null;
                if (entrenador.getNombres() == null) {
                    text.setText("REGISTRAR UN ENTRENADOR");
                    registrar.setEnabled(true);
                    Toast.makeText(getApplicationContext(), "NO HAY MODELO", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "HAY MODELO", Toast.LENGTH_SHORT).show();
                    Picasso.get()
                            .load(entrenador.getImagen())
                            .into(imagen);
                    nombres.setText("Nombre: " + entrenador.getNombres());
                    pueblo.setText("Pueblo: " + entrenador.getPueblo());
                }
            }

            @Override
            public void onFailure(Call<modelo> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
            }
        });

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), registrarEntrenadorActivity.class);
                startActivity(intent);
            }
        });

        Button CrearPokemon = findViewById(R.id.CrearPokemon);
        Button verPokemon = findViewById(R.id.verPokemon);
        Button capturarPokemon = findViewById(R.id.capturarPokemon);

        CrearPokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), crearPokemonActivity.class);
                startActivity(intent);
            }
        });
        verPokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), verPokemonCapActivity.class);
                startActivity(intent);
            }
        });
        capturarPokemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), verPokemonActivity.class);
                startActivity(intent);
            }
        });
    }
}
package com.example.examen_final;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface servicio {


    @GET("pokemones/{ID}")
    Call<pokemon> getPoke(@Path("ID") String id);

    @GET("N000284164")
    Call<modelo> getEntrenador();

    @POST("N000284164")
    Call<Void> POST(@Body modelo entrenador);

    @POST("N000284164/crear")
    Call<Void> POSTcrear(@Body pokemon pok);

    @GET("N000284164")
    Call<List<pokemon>> getPokemones();

    @POST("entrenador/N000284164/pokemon")
    Call<pokemon> POSTcap(@Body pokemon cap);

    @GET("N000284164/pokemones")
    Call<List<pokemon>> getPokemonesCap();
}

package com.example.examen_final;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class apadterOke extends RecyclerView.Adapter<apadterOke.AdapterCotacViewHolder> {


    List<pokemon> list;
    Context mContext;


    public apadterOke(List<pokemon> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterCotacViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterCotacViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_poke, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCotacViewHolder holder, int position) {

        holder.SetInfo(list.get(position));

        pokemon aClass = list.get(position);

        String ID = String.valueOf(aClass.getId());


        holder.ver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, detallePokemonActivity.class);
                intent.putExtra("Poke", ID);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class AdapterCotacViewHolder extends RecyclerView.ViewHolder {

        ImageView imageURl;
        TextView name;
        TextView tipo;
        Button ver;

        public AdapterCotacViewHolder(@NonNull View itemView) {
            super(itemView);
            imageURl = itemView.findViewById(R.id.imageURL);
            name = itemView.findViewById(R.id.name);
            tipo = itemView.findViewById(R.id.tipo);
            ver = itemView.findViewById(R.id.ver);
        }

        void SetInfo(pokemon aClass) {

            Log.e("name", aClass.getNombre());
            Log.e("tipo", aClass.getTipo());
            // Log.e("image", aClass.getImagen());
            Log.e("image_ur", aClass.getUrl_imagen());
            String im = "https://upn.lumenes.tk" + aClass.getUrl_imagen();
            Picasso.get()
                    .load(im)
                    .into(imageURl);
            name.setText(aClass.getNombre());
            tipo.setText(aClass.getTipo());
        }
    }
}
